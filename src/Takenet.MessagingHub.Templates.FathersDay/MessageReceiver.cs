using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Lime.Protocol;
using Takenet.MessagingHub.Client;
using Takenet.MessagingHub.Client.Listener;
using Takenet.MessagingHub.Client.Sender;
using System.Linq;
using Lime.Messaging.Contents;

namespace Takenet.MessagingHub.Templates.FathersDay
{
    public class MessageReceiver : IMessageReceiver
    {
        private readonly IMessagingHubSender _sender;
        private readonly Settings _settings;

        public MessageReceiver(IMessagingHubSender sender, Settings settings)
        {
            _sender = sender;
            _settings = settings;
        }

        public async Task ReceiveAsync(Message message, CancellationToken cancellationToken)
        {
            //Console.WriteLine($"From: {message.From} \tContent: {message.Content}");
            
            var itensMatched = new List<MessageRepostitoryItem>();
            var content = message.Content.ToString();

            foreach (var item in _settings.MessageRepository)
            {
                var match = Regex.Match(content, item.Key, RegexOptions.IgnoreCase);
                if (match.Success)
                {
                    itensMatched.Add(item);
                }
            }
            
            if(itensMatched.Count == 1)
            {
                var item = itensMatched.First();

                var responseMessage = new Message
                {
                    Id = EnvelopeId.NewId(),
                    To = message.From,
                    Content = new MediaLink
                    {
                        Uri = new Uri(message.From.Domain == "0mn.io" ? item.OmniUri : item.AmazonUri),
                        Text = item.Message,
                        Type = new MediaType (MediaType.DiscreteTypes.Video, "mp4", null),
                        Size = 1
                    }
                };

                await _sender.SendMessageAsync(responseMessage);
                return;
            }

            if(itensMatched.Count > 1)
            {
                await _sender.SendMessageAsync(_settings.ConflictMessage, message.From, cancellationToken);
                return;
            }
            
            await _sender.SendMessageAsync(_settings.DefaultMessage, message.From, cancellationToken);
        }
    }
}
