﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Takenet.MessagingHub.Templates.FathersDay
{
    public class Settings
    {
        public Broadcast Broadcast { get; set; }
        public MessageRepostitoryItem[] MessageRepository { get; set; }
        public string DefaultMessage { get; set; }
        public string ConflictMessage { get; set; }
    }

    public class MessageRepostitoryItem
    {
        public string Key { get; set; }
        public string OmniUri { get; set; }
        public string AmazonUri { get; set; }
        public string Message { get; set; }
    }

    public class Broadcast
    {
        public string Message { get; set; }
        public string[] Targets { get; set; }
    }
}
