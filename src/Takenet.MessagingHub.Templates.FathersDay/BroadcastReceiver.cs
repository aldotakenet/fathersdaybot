﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Lime.Protocol;
using Takenet.MessagingHub.Client;
using Takenet.MessagingHub.Client.Listener;
using Takenet.MessagingHub.Client.Sender;

namespace Takenet.MessagingHub.Templates.FathersDay
{
    public class BroadcastReceiver : IMessageReceiver
    {
        private readonly IMessagingHubSender _sender;
        private readonly Settings _settings;

        public BroadcastReceiver(IMessagingHubSender sender, Settings settings)
        {
            _sender = sender;
            _settings = settings;
        }

        public async Task ReceiveAsync(Message envelope, CancellationToken cancellationToken = default(CancellationToken))
        {
            foreach(var item in _settings.Broadcast.Targets)
            {
                await _sender.SendMessageAsync(_settings.Broadcast.Message, Node.Parse(item), cancellationToken);
            }
        }
    }
}
